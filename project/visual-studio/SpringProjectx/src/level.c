void levelChange(int levelIndex) {
	switch (levelIndex) {// As the player proceeds to the next level the files containing the data for the map and entites is loaded.
	case 1:
		initStage("data/map01.dat", "data/ents01.dat");//initStage function is used to load the data.
		break;
	case 2:
		initStage("data/map02.dat", "data/ents02.dat");
		break;
	case 3:
		initStage("data/map03.dat", "data/ents03.dat");
		break;
	case 4:
		exit(1);// when the player finishes all 4 levels successfully, the game ends.
		break;
	}
}